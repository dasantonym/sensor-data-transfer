import serial
import argparse
import threading

from pythonosc import osc_message_builder
from pythonosc import udp_client
from pythonosc import dispatcher
from pythonosc import osc_server

if __name__ == "__main__":

    def set_relay(unused_addr, number, state):
        message = 'R:'+str(int(number))+':'+str(int(state))+'-'
        print('poop ' + message)
        ser.write(message.encode('UTF-8'))

    def set_osc_server():
        server = osc_server.ThreadingOSCUDPServer((args.ip, args.port), dispatcher)
        print("osc server running on {}".format(server.server_address))
        server.serve_forever()

    def read_serial():
        while True:
            data = ser.readline().decode('ASCII')
            print("debug serial: " + data)
            msgblocks = data.split('-')
            for rawmsg in msgblocks:
                msgparts = rawmsg.split(':')
                if len(msgparts) == 3 and msgparts[0] == 'S':
                    msg = osc_message_builder.OscMessageBuilder(address="/sensor")
                    msg.add_arg(float(msgparts[1]))
                    msg.add_arg(float(msgparts[2]))
                    msg = msg.build()
                    client.send(msg)

    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="127.0.0.1",
                        help="ip to listen for osc packets")
    parser.add_argument("--port", type=int, default=8000,
                        help="port to listen for osc packets")
    parser.add_argument("--targetip", default="127.0.0.1",
                        help="target ip for sending osc")
    parser.add_argument("--targetport", type=int, default=8888,
                        help="target port for sending osc")
    parser.add_argument("--serialdevice", default="/dev/tty.usbmodem1411",
                        help="path to arduino serial device")
    parser.add_argument("--serialspeed", type=int, default=9600,
                        help="baud rate for arduino serial")
    args = parser.parse_args()

    client = udp_client.UDPClient(args.targetip, args.targetport)

    dispatcher = dispatcher.Dispatcher()
    dispatcher.map("/relay", set_relay)

    ser = serial.Serial(args.serialdevice, args.serialspeed)

    ts = threading.Thread(target=read_serial)
    ts.start()

    to = threading.Thread(target=set_osc_server)
    to.start()

