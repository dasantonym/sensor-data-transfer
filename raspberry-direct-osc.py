import argparse
import time
import atexit
import threading

import RPi.GPIO as GPIO

from pythonosc import osc_message_builder
from pythonosc import udp_client
from pythonosc import dispatcher
from pythonosc import osc_server


class PirSensor:
    def __init__(self, id, pin):
        self.id = id
        self.pin = pin
        self.last_set = int(round(time.time() * 1000))
        self.state = False


class RelaySwitch:
    def __init__(self, id, pin):
        self.id = id
        self.pin = pin
        self.state = False


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="127.0.0.1",
                        help="ip to listen for osc packets")
    parser.add_argument("--port", type=int, default=8000,
                        help="port to listen for osc packets")
    parser.add_argument("--targetip", default="127.0.0.1",
                        help="target ip for sending osc")
    parser.add_argument("--targetport", type=int, default=8888,
                        help="target port for sending osc")
    parser.add_argument("--sensoroffset", type=int, default=0,
                        help="shift sensor ids by this")
    parser.add_argument("--relayoffset", type=int, default=0,
                        help="shift relay ids by this")
    args = parser.parse_args()

    RELAY_ON = False
    RELAY_OFF = True
    SENSOR_DELAY = 2000

    sensor_pin_list = [11, 9, 10, 22, 27, 17, 4]
    relay_pin_list = [7, 8, 25, 24, 23, 18, 15]

    sensor_list = []
    relay_list = []

    GPIO.setmode(GPIO.BCM)

    atexit.register(GPIO.cleanup)

    client = udp_client.UDPClient(args.targetip, args.targetport)
    dispatcher = dispatcher.Dispatcher()

    for count in range(0, len(sensor_pin_list)):
        p = PirSensor(count+1+args.sensoroffset, sensor_pin_list[count])
        print("setup sensor: " + str(p.id) + " on pin: " + str(p.pin))
        GPIO.setup(p.pin, GPIO.IN)
        sensor_list.append(p)

    for count in range(0, len(relay_pin_list)):
        p = RelaySwitch(count+1+args.relayoffset, relay_pin_list[count])
        print("setup relay: " + str(p.id) + " on pin: " + str(p.pin))
        GPIO.setup(p.pin, GPIO.OUT)
        GPIO.output(p.pin, RELAY_OFF)
        relay_list.append(p)

    def set_relay(addr, number, state):
        print("set relay: " + str(number) + " state: " + str(state))
        for s in range(0, len(sensor_list)):
            if relay_list[s].id == number:
                if int(state) == 1:
                    print('relay on: ' + str(relay_list[s].id))
                    GPIO.output(relay_list[s].pin, RELAY_ON)
                else:
                    print('relay off: ' + str(relay_list[s].id))
                    GPIO.output(relay_list[s].pin, RELAY_OFF)

    def set_osc_server():
        server = osc_server.ThreadingOSCUDPServer((args.ip, args.port), dispatcher)
        print("osc server running on {}".format(server.server_address))
        server.serve_forever()

    def read_gpio():
        while True:
            now = int(round(time.time() * 1000))
            for s in range(0, len(sensor_list)):
                if GPIO.input(sensor_list[s].pin) \
                        and sensor_list[s].state is False \
                        and now-sensor_list[s].last_set > SENSOR_DELAY:
                    print("PIR on: " + str(sensor_list[s].id))
                    sensor_list[s].last_set = now
                    sensor_list[s].state = True
                    msg = osc_message_builder.OscMessageBuilder(address="/sensor")
                    msg.add_arg(sensor_list[s].id)
                    msg.add_arg(1)
                    msg = msg.build()
                    client.send(msg)
                elif sensor_list[s].state is True \
                        and now-sensor_list[s].last_set > SENSOR_DELAY:
                    print("PIR off: " + str(sensor_list[s].id))
                    sensor_list[s].last_set = now
                    sensor_list[s].state = False
                    msg = osc_message_builder.OscMessageBuilder(address="/sensor")
                    msg.add_arg(sensor_list[s].id)
                    msg.add_arg(0)
                    msg = msg.build()
                    client.send(msg)

    dispatcher.map("/relay", set_relay)

    to = threading.Thread(target=set_osc_server)
    to.start()

    tg = threading.Thread(target=read_gpio)
    tg.start()
